// main
package main

import (
	"net/http"

	_ "strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"

	"io"
	"log"
	"os"

	"ap02_shain_kanri/module/common/comconst"
	"ap02_shain_kanri/module/common"
	"ap02_shain_kanri/module/model"

	"github.com/signintech/gopdf"
)

// メイン
func main() {
	router := gin.Default()
	router.Static("/static", "./static")
	router.LoadHTMLGlob("templates/*")

	common.CreateSession(router)

	router.Any("/", model.LoginHandler)
	router.Any("/logout", model.LogoutHandler)
	router.Any("/menu", model.MenuHandler)
	router.Any("/user_list", model.UserListHandler)
	router.Any("/user_add", model.UserAddHandler)
	router.Any("/user_upd", model.UserUpdHandler)
	router.Any("/user_del", model.UserDelHandler)
	router.Any("/face_image/:file_name", model.FaceImageHandler)
	
	// 検証用関数 start
	router.Any("/download1", DownloadRoute1)
	router.Any("/download2", DownloadRoute2)
	router.Any("/user_list_pdf", UserListPdfRoute)
	// 検証用関数 end
	
	router.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusOK, "404.tmpl", gin.H{})
	})

	router.Run()
}

// 検証用関数 start
// ダウンロードパターン１
func DownloadRoute1(c *gin.Context) {
	f, err := os.Open(comconst.CURRENT_DIR + "/static/default.jpg")
	if err != nil {
		//		return err
	}
	defer f.Close()

	fileInfo, err := f.Stat()
	if err != nil {
		//		return err
	}

	http.ServeContent(c.Writer, c.Request, fileInfo.Name(), fileInfo.ModTime(), f)
}

// ダウンロードパターン２
func DownloadRoute2(c *gin.Context) {
	f, err := os.Open(comconst.CURRENT_DIR + "/static/default.jpg")
	if err != nil {
		//		return err
	}
	defer f.Close()

	fileInfo, err := f.Stat()
	if err != nil {
		//		return err
	}

	c.Header("Content-Disposition", "attachment; filename="+fileInfo.Name())
	//c.Header("Content-Type", "application/xls")
	c.Header("Content-Type", "image/jpeg")

	io.Copy(c.Writer, f)
}

// PDF生成
func UserListPdfRoute(c *gin.Context) {
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4
	pdf.AddPage()
	err := pdf.AddTTFFont("IPAexゴシック", "C:/Users/storm/go/src/ap02_shain_kanri/static/ipaexg.ttf")
	if err != nil {
		log.Print(err.Error())
		return
	}

	pdf.Image("C:/Users/storm/go/src/ap02_shain_kanri/static/default.jpg", 200, 50, nil) //print image
	err = pdf.SetFont("IPAexゴシック", "", 14)
	if err != nil {
		log.Print(err.Error())
		return
	}

	pdf.SetLineWidth(0.5)
	pdf.SetFillColor(100, 100, 255)
	pdf.RectFromUpperLeftWithStyle(5, 5, 100, 100, "FD")
	pdf.SetFillColor(0, 0, 0)

	pdf.Cell(nil, "漢字①")

	pdf.WritePdf("C:/Users/storm/go/src/ap02_shain_kanri/tmp/hello.pdf")
}
// 検証用関数 end
