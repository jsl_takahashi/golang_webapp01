package main

import (
	"bytes"
	"database/sql"
	"io/ioutil"
	"log"
	"os"

	_ "github.com/lib/pq"
)

/* DDL
CREATE TABLE files (
  id numeric not null,
  name varchar(200) not null,
  contents bytea,
  constraint pk_id primary key(id)
);
*/

var DRIVER_NAME = "postgres"
var DB_CONNECT_STRING = "postgres://***:***@127.0.0.1/***_db?sslmode=disable"

func main() {
	// データベースにバイナリデータを登録する
	inputFileDir := "C:/home/knowledge/go/src/00_work/data/" // 入力ファイルのディレクトリ
	inputFileName := "02.jpg"                                // 入力ファイル名
	insertBinaryData(inputFileDir, inputFileName)

	// データベースからバイナリデータを抽出する
	id := 1                                                        // 検索キー
	outputFileDir := "C:/home/knowledge/go/src/00_work/data/temp/" // バイナリデータの出力フォルダ
	extractBinaryData(id, outputFileDir)
}

// データベースにバイナリデータを登録する
func insertBinaryData(fileDir string, fileName string) {
	db, err := sql.Open(DRIVER_NAME, DB_CONNECT_STRING)
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	contents, err := ioutil.ReadFile(fileDir + fileName)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(len(contents))

	tran, err := db.Begin()

	var query bytes.Buffer
	query.WriteString("insert into files values((select coalesce(max(id), 0)+1 from files), $1, $2)")
	_, err = db.Exec(query.String(), fileName, contents)

	if err != nil {
		log.Println(err)
		tran.Rollback()
		return
	}
	tran.Commit()
}

// データベースからバイナリデータを抽出する
func extractBinaryData(p_id int, fileDir string) {
	db, err := sql.Open(DRIVER_NAME, DB_CONNECT_STRING)
	if err != nil {
		log.Println(err)
		return
	}
	defer db.Close()

	var query bytes.Buffer
	query.WriteString("select id, name, contents from files where id = $1 order by id")
	rows, err := db.Query(query.String(), p_id)
	if err != nil {
		log.Println(err)
		return
	}
	var id string
	var name string
	var contents []byte
	rows.Next()
	err = rows.Scan(&id, &name, &contents)
	ioutil.WriteFile(fileDir+id+"_"+name, contents, os.ModePerm)
}
