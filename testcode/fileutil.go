package main

import (
	"errors"
	_ "fmt"
	"image/jpeg"
	"log"
	"os"
	"strconv"

	"github.com/nfnt/resize"
)

// 最大ファイルサイズ: 1MB
var MAX_FILE_SIZE = int64(1048576)

// イメージリサイズ後のサイズ: 100x100
var RESIZE_SIZE = uint(100)

func main() {

	dir, _ := os.Getwd()
	filePath := dir + "/data/" + "05.jpg"
	err := CheckImageFile(filePath)
	if err != nil {
		log.Fatal(err)
		return
	}

	ResizeImageFile(filePath)
}

// イメージファイルの妥当性をチェックする
func CheckImageFile(filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		// ファイルが開けない場合はエラー
		return err
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		// ファイルステータスが取得できない場合はエラー
		return err
	}

	if MAX_FILE_SIZE < fileinfo.Size() {
		// ファイルサイズオーバーの場合はエラー
		return errors.New("file size over. max size : " + strconv.FormatInt(MAX_FILE_SIZE, 10))
	}

	_, err = jpeg.Decode(file)
	if err != nil {
		// jpegファイル以外の場合はエラー
		return err
	}

	// チェック結果に問題がない場合はnilを返す
	return nil
}

// イメージファイルをリサイズする
func ResizeImageFile(filePath string) {
	file, _ := os.Open(filePath)
	img, _ := jpeg.Decode(file)
	file.Close()

	img = resize.Resize(RESIZE_SIZE, 0, img, resize.Lanczos3)
	out, _ := os.Create(filePath)
	jpeg.Encode(out, img, nil)
	out.Close()
}
