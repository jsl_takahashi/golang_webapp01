package main

import (
	"log"

	"github.com/signintech/gopdf"
)

// フォントファイルのパス
var FONT_FILE = "C:/home/knowledge/go/src/00_work/font/ipaexg.ttf"

// フォント名
var FONT_NAME = "IPAexゴシック"

// フォントサイズ
var FONT_SIZE = 14

// 顔写真のディレクトリ
var IMAGE_FILE_DIR = "C:/home/knowledge/go/src/00_work/data/"

// PDFの出力先
var OUTPUT_DIR = "C:/home/knowledge/go/src/00_work/data/"

func main() {
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: 595.28, H: 841.89}}) //595.28, 841.89 = A4
	pdf.AddPage()
	err := pdf.AddTTFFont(FONT_NAME, FONT_FILE)
	if err != nil {
		log.Println(err)
		return
	}
	err = pdf.SetFont(FONT_NAME, "", FONT_SIZE)
	if err != nil {
		log.Print(err.Error())
		return
	}
	pdf.SetLineWidth(0.8)

	// 社員情報リスト
	list := []map[string]string{}
	list = append(list, map[string]string{"id": "00001", "name": "社員一郎", "birthday": "1995/01/01", "gender": "男", "imageFile": IMAGE_FILE_DIR + "01.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員二郎", "birthday": "1995/01/02", "gender": "男", "imageFile": IMAGE_FILE_DIR + "02.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員三郎", "birthday": "1995/01/03", "gender": "男", "imageFile": IMAGE_FILE_DIR + "03.jpg"})
	list = append(list, map[string]string{"id": "00001", "name": "社員一郎", "birthday": "1995/01/01", "gender": "男", "imageFile": IMAGE_FILE_DIR + "01.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員二郎", "birthday": "1995/01/02", "gender": "男", "imageFile": IMAGE_FILE_DIR + "02.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員三郎", "birthday": "1995/01/03", "gender": "男", "imageFile": IMAGE_FILE_DIR + "03.jpg"})
	list = append(list, map[string]string{"id": "00001", "name": "社員一郎", "birthday": "1995/01/01", "gender": "男", "imageFile": IMAGE_FILE_DIR + "01.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員二郎", "birthday": "1995/01/02", "gender": "男", "imageFile": IMAGE_FILE_DIR + "02.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員三郎", "birthday": "1995/01/03", "gender": "男", "imageFile": IMAGE_FILE_DIR + "03.jpg"})
	list = append(list, map[string]string{"id": "00001", "name": "社員一郎", "birthday": "1995/01/01", "gender": "男", "imageFile": IMAGE_FILE_DIR + "01.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員二郎", "birthday": "1995/01/02", "gender": "男", "imageFile": IMAGE_FILE_DIR + "02.jpg"})
	list = append(list, map[string]string{"id": "00002", "name": "社員三郎", "birthday": "1995/01/03", "gender": "男", "imageFile": IMAGE_FILE_DIR + "03.jpg"})

	y := float64(50)
	move_y := float64(60)

	// ヘッダ描画 start
	pdf.SetFillColor(200, 200, 200)
	pdf.RectFromUpperLeftWithStyle(50, 30, 400, 50, "FD")
	pdf.RectFromUpperLeftWithStyle(50, 30, 50, 65, "FD")   // id
	pdf.RectFromUpperLeftWithStyle(100, 30, 100, 65, "FD") // name
	pdf.RectFromUpperLeftWithStyle(200, 30, 100, 65, "FD") // birthday
	pdf.RectFromUpperLeftWithStyle(300, 30, 70, 65, "FD")  // imageFile
	pdf.SetFillColor(0, 0, 0)

	pdf.SetY(33)
	// id
	pdf.SetX(53)
	pdf.Cell(nil, "ID")
	// name
	pdf.SetX(103)
	pdf.Cell(nil, "氏名")
	// birthday
	pdf.SetX(203)
	pdf.Cell(nil, "生年月日")
	// gender
	pdf.SetX(303)
	pdf.Cell(nil, "性別")
	// imageFIle
	pdf.SetX(375)
	pdf.Cell(nil, "顔写真")
	// ヘッダ描画 end

	for _, value := range list {
		log.Println(value)

		pdf.SetFillColor(255, 255, 255)
		pdf.RectFromUpperLeftWithStyle(50, y, 400, 65, "FD")
		pdf.RectFromUpperLeftWithStyle(50, y, 50, 65, "FD")   // id
		pdf.RectFromUpperLeftWithStyle(100, y, 100, 65, "FD") // name
		pdf.RectFromUpperLeftWithStyle(200, y, 100, 65, "FD") // birthday
		pdf.RectFromUpperLeftWithStyle(300, y, 70, 65, "FD")  // imageFile
		pdf.SetFillColor(0, 0, 0)

		pdf.SetY(y + 5)
		// id
		pdf.SetX(53)
		pdf.Cell(nil, value["id"])
		// name
		pdf.SetX(103)
		pdf.Cell(nil, value["name"])
		// birthday
		pdf.SetX(203)
		pdf.Cell(nil, value["birthday"])
		// gender
		pdf.SetX(303)
		pdf.Cell(nil, value["gender"])
		// imageFile
		pdf.Image(value["imageFile"], 380, y+2, nil)

		// 改行
		y += move_y
	}

	pdf.WritePdf(OUTPUT_DIR + "test.pdf")
}
