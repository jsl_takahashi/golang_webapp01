package model

import (
	"bytes"
	"net/http"

	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// ログイン処理
func LoginHandler(c *gin.Context) {
	if common.IsLogin(c) == true {
		c.Redirect(http.StatusMovedPermanently, "/menu")
		return
	}

	s := common.GetSession(c)
	method := c.Request.Method

	if method == "GET" {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "",
		})
		return
	}

	if method == "POST" {
		db, err := common.CreateConnection()
		if err != nil {
			c.HTML(http.StatusOK, "login.tmpl", gin.H{
				"message": err.Error(),
			})
			return
		}
		defer db.Close()

		var query bytes.Buffer
		query.WriteString("select")
		query.WriteString("  user_id")
		query.WriteString(" ,password")
		query.WriteString(" ,sei")
		query.WriteString(" ,mei")
		query.WriteString(" from gin_admin_user where user_id=$1 and password=$2")

		rows, err := db.Query(query.String(), c.PostForm("user_id"), c.PostForm("password"))

		if err != nil {
			c.HTML(http.StatusOK, "login.tmpl", gin.H{
				"message": err.Error(),
			})
			return
		}

		var user_id string
		var password string
		var sei string
		var mei string

		if rows.Next() {
			err = rows.Scan(&user_id, &password, &sei, &mei)
			s.Set("alive", true)
			s.Set("user", user_id+" "+sei+" "+mei)
			s.Save()

			c.HTML(http.StatusOK, "menu.tmpl", gin.H{
				"message": "",
				"user":    s.Get("user"),
			})
		} else {
			s.Clear()
			s.Save()

			c.HTML(http.StatusOK, "login.tmpl", gin.H{
				"message": "ログインエラーです",
			})
		}
		rows.Close()
	}
}
