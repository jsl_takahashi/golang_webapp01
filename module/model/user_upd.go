package model

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"ap02_shain_kanri/module/common/comconst"
	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// ユーザ更新処理
func UserUpdHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	s := common.GetSession(c)

	method := common.GetRequestMethod(c)
	if method == "POST" {
		command := c.PostForm("command")
		param_user_id := c.PostForm("user_id")

		if command == "" {
			db, err := common.CreateConnection()
			if err != nil {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message": err.Error(),
					"user":    s.Get("user"),
				})
				return
			}

			var query bytes.Buffer
			query.WriteString("select")
			query.WriteString("  user_id")
			query.WriteString(" ,sei")
			query.WriteString(" ,mei")
			query.WriteString(" ,seinengappi")
			query.WriteString(" ,ketsuekigata")
			query.WriteString(" ,face_image_path")
			query.WriteString(" from gin_user where user_id = $1")

			rows, err := db.Query(query.String(), param_user_id)
			if err != nil {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message": err.Error(),
					"user":    s.Get("user"),
				})
				return
			}
			defer db.Close()

			var user_id string
			var sei string
			var mei string
			var seinengappi string
			var ketsuekigata string
			var face_image_path string

			if rows.Next() {
				err = rows.Scan(&user_id, &sei, &mei, &seinengappi, &ketsuekigata, &face_image_path)
			}
			rows.Close()

			c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
				"user_id":         user_id,
				"user":            s.Get("user"),
				"sei":             sei,
				"mei":             mei,
				"seinengappi":     seinengappi,
				"ketsuekigata":    ketsuekigata,
				"face_image_path": face_image_path,
			})
		} else {
			user_id := c.PostForm("user_id")
			sei := c.PostForm("sei")
			mei := c.PostForm("mei")
			seinengappi := c.PostForm("seinengappi")
			ketsuekigata := c.PostForm("ketsuekigata")

			if user_id == "" || sei == "" || mei == "" || seinengappi == "" || ketsuekigata == "" {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message":      "全部入力してください。",
					"user":         s.Get("user"),
					"user_id":      user_id,
					"sei":          sei,
					"mei":          mei,
					"seinengappi":  seinengappi,
					"ketsuekigata": ketsuekigata,
				})
				return
			}

			//
			fmt.Println("update.")
			file, header, err := c.Request.FormFile("face_image_path")
			if file == nil {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message":      "全部入力してください。",
					"user":         s.Get("user"),
					"user_id":      user_id,
					"sei":          sei,
					"mei":          mei,
					"seinengappi":  seinengappi,
					"ketsuekigata": ketsuekigata,
				})
				return
			}

			filename := header.Filename
			uploadFile := comconst.UPLOAD_DIR + filename
			out, err := os.Create(uploadFile)

			if err != nil {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message":      "全部入力してください。",
					"user":         s.Get("user"),
					"user_id":      user_id,
					"sei":          sei,
					"mei":          mei,
					"seinengappi":  seinengappi,
					"ketsuekigata": ketsuekigata,
				})
				return
			}
			defer out.Close()

			_, err = io.Copy(out, file)
			if err != nil {
				log.Fatal(err)
			}
			//

			db, err := common.CreateConnection()
			if err != nil {
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message": err.Error(),
					"user":    s.Get("user"),
				})
				return
			}
			defer db.Close()
			tran, err := db.Begin()

			var query bytes.Buffer
			query.WriteString("update gin_user set user_id=$1,sei=$2,mei=$3,seinengappi=$4,ketsuekigata=$5,face_image_path=$6 where user_id=$7;")

			_, err = db.Exec(query.String(), user_id, sei, mei, seinengappi, ketsuekigata, uploadFile, user_id)

			if err != nil {
				tran.Rollback()
				c.HTML(http.StatusOK, "user_upd.tmpl", gin.H{
					"message": err.Error(),
					"user":    s.Get("user"),
				})
				return
			}
			tran.Commit()

			c.Redirect(http.StatusMovedPermanently, "/user_list")
		}
	}
}
