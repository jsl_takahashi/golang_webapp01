package model

import (
	"net/http"

	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// メニュー処理
func MenuHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	s := common.GetSession(c)
	c.HTML(http.StatusOK, "menu.tmpl", gin.H{
		"user": s.Get("user"),
	})
}
