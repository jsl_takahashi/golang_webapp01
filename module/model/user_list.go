package model

import (
	"bytes"
	"net/http"
	"strings"

	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// ユーザリスト処理
func UserListHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	db, err := common.CreateConnection()
	if err != nil {
		c.HTML(http.StatusOK, "user_list.tmpl", gin.H{
			"message": err.Error(),
		})
		return
	}
	defer db.Close()

	var query bytes.Buffer
	query.WriteString("select")
	query.WriteString("  user_id")
	query.WriteString(" ,sei")
	query.WriteString(" ,mei")
	query.WriteString(" ,seinengappi")
	query.WriteString(" ,ketsuekigata")
	query.WriteString(" ,face_image_path")
	query.WriteString(" from gin_user")
	query.WriteString(" order by user_id")

	rows, err := db.Query(query.String())

	if err != nil {
		c.HTML(http.StatusOK, "user_list.tmpl", gin.H{
			"message": err.Error(),
		})
		return
	}

	var user_id string
	var sei string
	var mei string
	var seinengappi string
	var ketsuekigata string
	var face_image_path string
	var face_image_file_name string
	list := []map[string]string{}
	for rows.Next() {
		err = rows.Scan(&user_id, &sei, &mei, &seinengappi, &ketsuekigata, &face_image_path)

		face_image_file_name = face_image_path[strings.LastIndex(face_image_path, "/")+1 : len(face_image_path)]

		//		file, _ := os.Open(face_image_path)
		//		file_data, _ := ioutil.ReadAll(file)
		//		faceData := base64.StdEncoding.EncodeToString(file_data)

		m := map[string]string{
			"user_id": user_id, "sei": sei, "mei": mei, "seinengappi": seinengappi,
			"ketsuekigata": ketsuekigata, "face_image_file_name": face_image_file_name}
		list = append(list, m)
	}
	rows.Close()

	s := common.GetSession(c)
	c.HTML(http.StatusOK, "user_list.tmpl", gin.H{
		"list": list,
		"user": s.Get("user"),
	})
}
