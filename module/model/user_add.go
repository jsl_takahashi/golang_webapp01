package model

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"ap02_shain_kanri/module/common"
	"ap02_shain_kanri/module/common/comconst"

	"github.com/gin-gonic/gin"
)

// ユーザ追加処理
func UserAddHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	s := common.GetSession(c)

	method := common.GetRequestMethod(c)
	if method == "POST" {
		command := c.PostForm("command")
		if command == "" {
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message": "",
				"user":    s.Get("user"),
			})
			return
		}

		sei := c.PostForm("sei")
		mei := c.PostForm("mei")
		seinengappi := c.PostForm("seinengappi")
		ketsuekigata := c.PostForm("ketsuekigata")

		if sei == "" || mei == "" || seinengappi == "" || ketsuekigata == "" {
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message":      "全部入力してください。",
				"user":         s.Get("user"),
				"sei":          sei,
				"mei":          mei,
				"seinengappi":  seinengappi,
				"ketsuekigata": ketsuekigata,
			})
			return
		}

		//
		fmt.Println("add.")
		file, header, err := c.Request.FormFile("face_image_path")
		if file == nil {
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message":      "全部入力してください。",
				"user":         s.Get("user"),
				"sei":          sei,
				"mei":          mei,
				"seinengappi":  seinengappi,
				"ketsuekigata": ketsuekigata,
			})
			return
		}

		filename := header.Filename
		uploadFile := comconst.UPLOAD_DIR + filename
		out, err := os.Create(uploadFile)

		if err != nil {
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message":      "全部入力してください。",
				"user":         s.Get("user"),
				"sei":          sei,
				"mei":          mei,
				"seinengappi":  seinengappi,
				"ketsuekigata": ketsuekigata,
			})
			return
		}
		defer out.Close()

		_, err = io.Copy(out, file)
		if err != nil {
			log.Fatal(err)
		}
		//

		db, err := common.CreateConnection()
		if err != nil {
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message": err.Error(),
				"user":    s.Get("user"),
			})
			return
		}
		defer db.Close()
		tran, err := db.Begin()

		var query bytes.Buffer
		query.WriteString("insert into gin_user values((select to_char(coalesce(max(to_number(user_id, '00000')), 0)+1, 'FM00000') from gin_user),$1,$2,$3,$4,$5);")

		_, err = db.Exec(query.String(), sei, mei, seinengappi, ketsuekigata, uploadFile)

		if err != nil {
			tran.Rollback()
			c.HTML(http.StatusOK, "user_add.tmpl", gin.H{
				"message": err.Error(),
				"user":    s.Get("user"),
			})
			return
		}
		tran.Commit()

		c.Redirect(http.StatusMovedPermanently, "/user_list")
	}
}
