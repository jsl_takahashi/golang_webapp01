package model

import (
	"net/http"

	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// ログアウト処理
func LogoutHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	s := common.GetSession(c)
	s.Clear()
	s.Save()

	c.HTML(http.StatusOK, "login.tmpl", gin.H{
		"message": "",
	})
}
