package model

import (
	"bytes"
	"net/http"

	"ap02_shain_kanri/module/common"

	"github.com/gin-gonic/gin"
)

// ユーザ削除処理
func UserDelHandler(c *gin.Context) {
	if common.IsLogin(c) == false {
		c.HTML(http.StatusOK, "login.tmpl", gin.H{
			"message": "ログインエラーです",
		})
		return
	}

	s := common.GetSession(c)

	method := common.GetRequestMethod(c)
	if method == "POST" {
		user_id := c.PostForm("user_id")

		db, err := common.CreateConnection()
		if err != nil {
			c.HTML(http.StatusOK, "user_list.tmpl", gin.H{
				"message": err.Error(),
				"user":    s.Get("user"),
			})
			return
		}
		defer db.Close()
		tran, err := db.Begin()

		var query bytes.Buffer
		query.WriteString("delete from gin_user where user_id = $1;")

		_, err = db.Exec(query.String(), user_id)

		if err != nil {
			tran.Rollback()
			c.HTML(http.StatusOK, "user_list.tmpl", gin.H{
				"message": err.Error(),
				"user":    s.Get("user"),
			})
			return
		}
		tran.Commit()

		c.Redirect(http.StatusMovedPermanently, "/user_list")
	}
}
