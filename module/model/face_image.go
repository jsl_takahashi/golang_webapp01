package model

import (
	"net/http"
	"os"
	"time"

	"ap02_shain_kanri/module/common/comconst"

	"github.com/gin-gonic/gin"
)

// 顔写真の読み込み
func FaceImageHandler(c *gin.Context) {
	f, err := os.Open(comconst.UPLOAD_DIR + c.Param("file_name"))
	if err != nil {
		f, _ = os.Open(comconst.CURRENT_DIR + "/static/default.png")
	}
	defer f.Close()

	fileInfo, err := f.Stat()
	if err != nil {
		f, _ = os.Open(comconst.CURRENT_DIR + "/static/default.png")
	}

	http.ServeContent(c.Writer, c.Request, fileInfo.Name(), time.Now(), f)
}
