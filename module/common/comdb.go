package common

import (
	"database/sql"

	"ap02_shain_kanri/module/common/comconst"
)

// データベース接続処理
func CreateConnection() (*sql.DB, error) {
	db, err := sql.Open("postgres", comconst.DB_CONN_STR)

	if err != nil {
		return db, err
	}

	err = db.Ping()
	if err != nil {
		return db, err
	}

	return db, nil
}
