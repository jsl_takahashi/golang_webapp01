package comconst

import "os"

// for local
//var DB_CONN_STR = "postgres://user:password@127.0.0.1/database?sslmode=disable"
//var UPLOAD_DIR = "C:/Users/hoge/go/src/ap02_shain_kanri/tmp/"
//var UPLOAD_DIR = "/Users/hoge/go/src/ap02_shain_kanri/tmp/"

// for heroku
var DB_CONN_STR = "postgres://hoge:fuga@ec2-23-23-248-162.compute-1.amazonaws.com/d3ip1b640kvgds?sslmode=disable"
var UPLOAD_DIR = os.TempDir() + "/"

// カレントディレクトリ
var CURRENT_DIR, _ = os.Getwd()
