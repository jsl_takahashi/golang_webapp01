package common

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// セッションを生成する
func CreateSession(router *gin.Engine) {
	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("AppSession", store))
}

// セッションを取得する
func GetSession(c *gin.Context) sessions.Session {
	return sessions.Default(c)
}

// ログイン済みかどうかをチェックする
// 未ログインの場合はメニュー画面を表示する
func IsLogin(c *gin.Context) bool {
	s := GetSession(c)
	if s.Get("alive") == nil {
		return false
	}
	return true
}

// リクエストメソッドを取得する
func GetRequestMethod(c *gin.Context) string {
	return c.Request.Method
}
