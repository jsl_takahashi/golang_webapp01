create table gin_user (
 user_id character varying(5) not null,
 sei character varying(20),
 mei character varying(20),
 seinengappi character varying(8),
 ketsuekigata character varying(2),
 face_image_path character varying(300),
 constraint pk_gin_user primary key(user_id)
);

insert into gin_user values ('00001','社員','一郎','19950101','A','');
insert into gin_user values ('00002','社員','二郎','19950201','B','');
insert into gin_user values ('00003','社員','三郎','19950301','O','');
