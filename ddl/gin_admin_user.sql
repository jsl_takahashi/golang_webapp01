create table gin_admin_user (
 user_id character varying(10) not null,
 password character varying(64) not null,
 sei character varying(20),
 mei character varying(20),
 constraint pk_gin_admin_user primary key(user_id)
);

insert into gin_admin_user values ('user01','user01p','管理','一郎');
insert into gin_admin_user values ('user02','user02p','管理','二郎');
